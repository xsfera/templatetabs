import {Component} from '@angular/core';
import {Setting} from 'src/app/setting';

@Component({
    selector: 'app-tabs',
    templateUrl: 'tabs.page.html',
    styleUrls: ['tabs.page.scss']
})
export class TabsPage {

    public menu;

    constructor(setting: Setting){
        this.menu = setting.menu;
    }

}
