export class Setting {
    public menu = {
        tab1: {
            title:  'Блог', // Надпись на вкладке
            url:    'https://xsfera.ru/category/общие/', // web-старница
            icon:   'create' // Иконка с сайта https://ionicons.com
        },
        tab2: {
            title:  'Портфолио',
            url:    'https://xsfera.ru/2019/04/portfolio/',
            icon:   '/assets/portfolio.svg' // иконка в SVG например с flaticon.com
        },
        tab3: {
            title:  'Контакты',
            url:    'https://xsfera.ru',
            icon:   'contacts'
        },
        // tab4: {
        //     title:  'Настройки',
        //     url:    'https://russian.servicepi.ru/youdo/personal',
        //     icon:   'settings'
        // }
    };
}

