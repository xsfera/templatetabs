import {Component} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {Setting} from 'src/app/setting';

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
    public srcUrl;
    private item;

    constructor(private sanitizer: DomSanitizer, private route: Router, setting: Setting) {
        const tab = this.route.url.substr(6, 4);
        this.item = setting.menu[tab];
        this.srcUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.item.url);
    }

    ionViewDidLeave() {
        this.srcUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.item.url);

    }
}
